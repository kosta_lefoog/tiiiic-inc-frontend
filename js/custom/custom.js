$( document ).ready(function() {
    $('#toggle').click(function() {
        $(this).toggleClass('active');
        $('#header-menu').toggleClass('open');
        $('.darkness').toggleClass('drop');
        $('.header__logo img').toggleClass('logo_drop');
        $('.main').toggleClass('no_scroll');
    });
});